import express from 'express';
import routes from './routes.ts';
const app = express();

app.use('./routes');

app.get('/', (req, res) => {
  return res.sendStatus(200).json({ok:'ok'})
})

app.listen(3000, () => {
  console.log('App running on port 3000')
})